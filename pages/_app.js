import App, { Container } from 'next/app';
import withApolloClient from '../lib/with-apollo-client';
import { ApolloProvider } from 'react-apollo';
import Layout from '../components/Layout';
import { PageTransition } from 'next-page-transitions';
import Router from 'next/link';
import '../styles.scss';

class Novabeds extends App {

    render() {
        const { Component, pageProps, apolloClient } = this.props;
        return (
            <Container>
                <ApolloProvider client={apolloClient}>
                    <PageTransition
                        timeout={300}
                        classNames="page-transition"
                        loadingDelay={500}
                        loadingTimeout={{
                            enter: 4000,
                            exit: 1000,
                        }}
                        loadingClassNames="loading-indicator"
                        skipInitialTransition={true}
                    >
                        <Component {...pageProps} key={Router.route} />
                    </PageTransition>
                </ApolloProvider>
            </Container>
        )
    }

}

export default withApolloClient(Novabeds);