
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import { compose } from "recompose";
import { homeQuery } from '../graphql/index';
import Grid from '../components/Grid';
import Section from "../containers/Section";
import TextComponent from "../containers/section/TextComponent";
import MediaComponent from "../containers/section/MediaComponent";
import GalleryComponent from "../containers/section/GalleryComponent";
import FormComponent from "../containers/section/FormComponent";
import Layout from "../components/Layout";

const Index = ({ data }) => {

  const { page } = data || [];

  const { title, description, keywords } = page || '';
  const hero = page ? page.sections.find(item => item.name == 'hero') : [];
  const proudly = page ? page.sections.find(item => item.name == 'proudly') : [];
  const channels = page ? page.sections.find(item => item.name == 'channels') : [];
  const booking_engine = page ? page.sections.find(item => item.name == 'booking_engine') : [];
  const property_management = page ? page.sections.find(item => item.name == 'property_management') : [];


  return (
    <Layout title={title} description={description} keywords={keywords}>
      <Section className={`hero ${hero.cssClass} o-hidden`}>
        <TextComponent className="hero__text text__component text__component-1-2" title={hero.title} content={hero.content} isTitle />
        <FormComponent className="hero__form form__component form__component-1-2 p-rel" />
        <Grid />
      </Section>

      <Section className={`proudly ${proudly.cssClass}`}>
        <TextComponent className="text__component text__component-2 text__center" content={proudly.content} />
      </Section>

      <Section className={`channels ${channels.cssClass}`}>
        <TextComponent className="text__component text__component-1-2" subtitle={channels.subtitle} title={channels.title} content={channels.content} />
        <MediaComponent className="media__component media__component-1-2" media={channels.media} />
        <Grid />
      </Section>

      <Section className={`booking_engine ${booking_engine.cssClass}`} reverse>
        <TextComponent className="text__component text__component-1-2" subtitle={booking_engine.subtitle} title={booking_engine.title} content={booking_engine.content} />
        <MediaComponent className="media__component media__component-1-2" media={booking_engine.media} />
        <Grid />
      </Section>

      <Section className={`property_management ${property_management.cssClass}`}>
        <TextComponent className="text__component text__component-1-2" subtitle={property_management.subtitle} title={property_management.title} content={property_management.content} />
        <GalleryComponent className="media__component media__component-1-2" media={property_management.media} />
      </Section>

    </Layout>
  );
}


export default compose(
  graphql(gql(homeQuery), {
    props: ({ data }) => ({ data })
  })
)(Index);