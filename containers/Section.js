import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { TweenMax } from 'gsap';

const Section = props => {

    const [ref, inView, entry] = useInView({
        /* Optional options */
        triggerOnce: true,
        threshold: 0,
    })

    useEffect(() => {
        if (inView) {
            
        }
    }, [inView])

    return (
        <section className={props.className} ref={ref}>
            <div className={`site__container ${props.reverse ? 'reverse' : ''}`}>
                {props.children}
            </div>
        </section>
    )

}

export default Section;