import React, { Component, useState } from 'react';
import { createForm, createFactory, createField } from 'micro-form';
import fetch from 'isomorphic-fetch';
import ProgressButton from '../components/ProgressButton';

const ContactForm = () => {

    const [submitting, setSubmitting] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [data, setData] = useState({});


    const submitForm = (data) => {
        setSubmitting(true);
        fetch(`${process.env.API_URL}/contacts`, {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((res) => {
            if (res.status === 200) {
                setSubmitted(true);
                setSubmitting(false);
            }
        })

    }

    const Form = createForm(({ state, valid, validateForm, getPayload, resetForm, ...props }) => (
        <form onSubmit={e => {
            e.preventDefault()

            if (validateForm()) {
                const data = getPayload();
                setData(data);
                submitForm(data);
            }
        }}>
            {props.children}
        </form>
    ))

    const Input = createFactory(({ name, value, valid, validateField, updateField, ...userDefinedProps }) => (
        <div>
            <input name={name} value={value} type={userDefinedProps.type} placeholder={userDefinedProps.placeholder} className="form-control" onChange={e => {
                updateField(e.target.value)
            }} />
        </div>
    ))

    const Email = createField({
        name: 'email',
        initialValue: ''
    })(({ name, value, valid, updateField, validateField, ...userDefinedProps }) => (
        <div>
            <input name={name} value={value} type='email' placeholder={userDefinedProps.placeholder} className="form-control" onChange={e => {
                updateField(e.target.value)
            }} />
        </div>
    ))



    return (

        <Form>
            {!submitted && 
                <div>
                    <h2 className="text__center">
                        Request a free consultation <br /> <strong>let’s chat!</strong>
                    </h2>
                    <div className="form-group">
                        <Email placeholder="Email" />
                    </div>
                    <div className="form-group">
                        <Input
                            name='name'
                            type='text'
                            placeholder='Name'
                            validate={val => val !== ''}
                            label='Name'
                        />
                    </div>
                    <div className="form-group">
                        <Input
                            name='phone'
                            type='text'
                            placeholder='Phone Number'
                            validate={val => val !== ''}
                            label='Phone Number'
                        />
                    </div>
                    <div className="form-group">
                        <Input
                            name='company'
                            type='text'
                            placeholder='Company'
                            validate={val => val !== ''}
                            label='Company'
                        />
                    </div>
                    <div className="form-group">
                        <Input
                            name='country'
                            type='text'
                            placeholder='Country'
                            validate={val => val !== ''}
                            label='Country'
                        />
                    </div>
                    <div className="form-group">
                        <ProgressButton
                            formNoValidate={true}
                            inProgress={submitting}
                            inProgressText='Submitting...'
                            isDone={submitted}
                            isDoneText='Submitted'>
                            Save and continue
                            </ProgressButton>
                    </div>
                </div>
            }
            {submitted && 
                <div>
                    <p>
                        An email confirm has been sent to <strong>{data.email}</strong>, Our customer care will take care about your enquire
                    </p>
                </div>
            }
        </Form>

    )

}

export default ContactForm