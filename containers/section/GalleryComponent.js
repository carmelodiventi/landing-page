import { useEffect, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import { TimelineLite } from 'gsap';

const GalleryComponent = props => {

    const [ref, inView, entry] = useInView({
        triggerOnce: true,
        threshold: 0.5,
    });

    const [currentIndex, setCurrentIndex] = useState(0);
    const [translateValue, setTranslateValue] = useState(0);

    useEffect(() => {
        if (inView) {
            let medias = entry.target.querySelectorAll('.gallery--element');
            let tl = new TimelineLite;
            tl.to(entry.target, 0.3, { y: 0, opacity: 1 })
            medias.forEach(item => tl.fromTo(item, 0.3, { y: 100, autoAlpha: 0 }, { y: 0, autoAlpha: 1 }))

        }
    }, [inView]);

    const goToPrevSlide = () => {
        if (currentIndex === 0) {
            return;
        }
        let index = currentIndex - 1;
        let translate = translateValue + slideWidth();
        setCurrentIndex(index);
        setTranslateValue(translate);
    }

    const goToNextSlide = () => {
        if (currentIndex === props.media.length - 1) {
            setCurrentIndex(0)
            setTranslateValue(0);
            return;
        }
        let index = currentIndex + 1;
        let translate = translateValue + -(slideWidth());
        
        setCurrentIndex(index);
        setTranslateValue(translate);
    }

    const slideWidth = () => {
        return document.querySelector('.slide').clientWidth;
    }

    return (
        <div className={props.className} ref={ref}>
            <div className="slider">
                <div className="slider-wrapper"
                    style={{
                        transform: `translateX(${translateValue}px)`,
                        transition: 'transform ease-out 0.45s'
                    }}>
                    {props.media &&
                        props.media.map((item, index) => <div className={`gallery--element media--${index} slide`} key={item.name}><img src={`${process.env.API_URL}/${item.url}`} alt={item.name} /></div>)
                    }
                </div>
                <div className="arrows">
                    <div className="arrow arrow-prev" onClick={goToPrevSlide}>
                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M-5.02681e-07 11.5L20 -1.30353e-06L6.22222 11.5L20 23L-5.02681e-07 11.5Z" fill="#C4C4C4" />
                        </svg>
                    </div>
                    <div className="arrow arrow-next" onClick={goToNextSlide}>
                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20 11.5L-2.49548e-06 23L13.7778 11.5L-1.49012e-06 -8.74228e-07L20 11.5Z" fill="#C4C4C4" />
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default GalleryComponent;
