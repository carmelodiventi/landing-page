import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { TweenMax } from 'gsap';
import ContactForm from "../ContactForms";

const FormComponent = props => {

    const [ref, inView, entry] = useInView({
        triggerOnce: true,
        threshold: 0.5,
    });

    useEffect(() => {
        if (inView) {
            TweenMax.to(entry.target, .5, { y: 0, autoAlpha:1 });
        }
    }, [inView]);

    return(
        <div className={props.className} ref={ref}>
          <ContactForm />
          <div className="hero__back hero--back-1"></div>
          <div className="hero__back hero--back-2"></div>
          <div className="hero__back hero--back-3"></div>
        </div>
    )

}

export default FormComponent;