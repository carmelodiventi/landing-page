import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { TimelineLite } from 'gsap';

const MediaComponent = props => {

    const [ref, inView, entry] = useInView({
        triggerOnce: true,
        threshold: 0.5,
    });

    useEffect(() => {
        if (inView) {
            let medias = entry.target.querySelectorAll('.media--element');
            let tl = new TimelineLite;
            tl.to(entry.target, 0.3, { y: 0, autoAlpha: 1 })
            medias.forEach(item => tl.fromTo(item, 0.3, { y: 50, autoAlpha: 0 }, { y: 0, autoAlpha: 1 }))

        }
    }, [inView]);

    return (
        <div className={props.className} ref={ref}>
            {props.media &&
                props.media.map((item, index) => <div className={`media--element media--${index}`} key={item.name}><img src={`${process.env.API_URL}/${item.url}`} alt={item.name} /></div>)
            }
        </div>
    )

}

export default MediaComponent;
