import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { TimelineLite } from 'gsap';
import { Converter } from 'showdown';

const TextComponent = props => {

    const [ref, inView, entry] = useInView({
        triggerOnce: true,
        threshold: 0.5,
    });

    const converter = new Converter;
    const { title, subtitle, content, isTitle } = props;

    useEffect(() => {
        if (inView) {
            const subtitle = entry.target.querySelector('.text__component__subtitle > p');
            const title = entry.target.querySelector('.text__component__title > p');
            const content = entry.target.querySelector('.text__component__paragraph > p');
            const underlined = entry.target.querySelector('.text__component__paragraph > p > strong::after');
            const tl = new TimelineLite;

            if (title) {
                tl.to(title, .5, { y: 0, autoAlpha: 1 });
            }
            if (content) {
                tl.to(content, .5, { y: 0, autoAlpha: 1 });
            }
            if (subtitle) {
                tl.to(subtitle, .5, { y: 0, autoAlpha: 1 });
            }
            if (underlined) {
                tl.to(underlined, .5, { width: '100%', autoAlpha: 1 });
            }
        }
    }, [inView]);

    return (
        <div className={props.className} ref={ref}>
            {subtitle && <h4 className="text__component__subtitle" dangerouslySetInnerHTML={{ __html: converter.makeHtml(props.subtitle) }} />}
            {title && isTitle && <h1 className="text__component__title" dangerouslySetInnerHTML={{ __html: converter.makeHtml(props.title) }} />}
            {title && !isTitle && <h2 className="text__component__title" dangerouslySetInnerHTML={{ __html: converter.makeHtml(props.title) }} />}
            {content && <div className="text__component__paragraph" dangerouslySetInnerHTML={{ __html: converter.makeHtml(props.content) }} />}
        </div>
    )

}

export default TextComponent;
