import Link from "next/link";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import { compose } from "recompose";
import { widgetQuery } from '../graphql/index';
import { Converter } from 'showdown';

const Footer = props => {
  
    const converter = new Converter;
    const widgets = props.data.widgets ? props.data.widgets.filter( item => item.position === 'footer' ) : [];

    return (
        <footer className="site__footer">
            <div className="site__container">
                {widgets.map(widget => 
                    <div key={widget.title}>
                        {widget.title && <h4 dangerouslySetInnerHTML={{ __html: converter.makeHtml(widget.title) }}/>}
                        {widget.content && <div dangerouslySetInnerHTML={{ __html: converter.makeHtml(widget.content) }}/>}
                    </div>
                )}
            </div>
        </footer>
    )
}


export default compose(
    graphql(gql(widgetQuery), {
      props: ({ data }) => ({ data })
    })
  )(Footer);