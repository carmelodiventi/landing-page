const ProgressButton = props => {

    return(
        <button className={props.isDone ? 'success' : null}>
            {!props.inProgress && !props.isDone && props.children}
            {props.inProgress && !props.isDone && props.inProgressText}
            {props.isDone && !props.inProgress && props.isDoneText}
        </button>
    )

}

export default ProgressButton;