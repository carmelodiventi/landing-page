import Head from 'next/head';
import Header from "./Header";
import Footer from "./Footer";

const Layout = props => {

    return (
        <React.Fragment>
            <Head>
                <title> {`${props.title} | Novabeds`} </title>
                <meta name="description" content={props.description} />
                <meta name="keywords" content={props.keywords} />
                <script dangerouslySetInnerHTML={{
                    __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-PD45QTQ')`}} />

            </Head>
            <Header />
            <main className="site__content">
                {props.children}
            </main>
            <Footer />
            <noscript dangerouslySetInnerHTML={{__html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PD45QTQ" height="0" width="0" style="display:none;visibility:hidden;"></iframe>`}} />
        </React.Fragment>
    )

}

export default Layout;