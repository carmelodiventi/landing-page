export const homeQuery = `query{
  page(id : "5db8b3decd7d1e5d4eed10ed"){
    id,
    title,
    name,
    description,
    keywords,
    sections{
      id,
      name,
      content,
      title,
      subtitle,
      cssClass,
      media{
        id,
        url,
        name
      }
    }
  }
}`;

export const widgetQuery = `query{
  widgets{
    name,
    title,
    content,
    position
  }
}`;